package com.yudha.testandroid;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private AppGraph appGraph;

    public static App get(Context context){
        return (App)context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appGraph = Injector.create(this);
        appGraph.inject(this);
    }

    AppGraph getInjector(){
        return appGraph;
    }
}
