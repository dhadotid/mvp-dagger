package com.yudha.testandroid;

import com.yudha.testandroid.connection.api.ApiModule;
import com.yudha.testandroid.repository.RepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(
        modules = {
                AppModule.class,
                ApiModule.class,
                RepositoryModule.class
        }
)

@Singleton
public interface AppComponent extends AppGraph{
}
