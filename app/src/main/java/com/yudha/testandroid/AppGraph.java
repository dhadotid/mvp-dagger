package com.yudha.testandroid;

import com.yudha.testandroid.ui.activity.home.HomeActivity;

public interface AppGraph {

    void inject(App app);

    void inject(HomeActivity homeActivity);
}
