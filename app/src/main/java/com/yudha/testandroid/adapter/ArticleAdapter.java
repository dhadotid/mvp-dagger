package com.yudha.testandroid.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yudha.testandroid.R;
import com.yudha.testandroid.base.BaseViewAdapter;
import com.yudha.testandroid.base.BaseViewHolder;
import com.yudha.testandroid.model.ItemModel;

import java.util.List;

/**
 * Created by yudha on 2019-07-07.
 */
public class ArticleAdapter extends BaseViewAdapter<ItemModel> {

    @Override
    public void setItems(List<ItemModel> items) {
        super.setItems(items);
    }

    @Override
    public BaseViewHolder<ItemModel> createHolder(ViewGroup parent, int viewType) {
        return new ArticleViewHolder(R.layout.layout_article, parent);
    }

    @Override
    public void bindHolder(BaseViewHolder holder, int position) {
        holder.bindView(getItem(position));
    }

    public static class ArticleViewHolder extends BaseViewHolder<ItemModel>{

        private ImageView ivArticle;
        private TextView tvTitle;


        public ArticleViewHolder(int resId, @NonNull ViewGroup parent) {
            super(resId, parent);

            ivArticle = itemView.findViewById(R.id.layout_article_imageView);
            tvTitle = itemView.findViewById(R.id.layout_article_textView);
        }

        @Override
        public void bindView(final ItemModel object) {
            Picasso.with(itemView.getContext())
                    .load(object.getImage())
                    .into(ivArticle);

            tvTitle.setText(object.getName());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(object.getLink()));
                    itemView.getContext().startActivity(browserIntent);
                }
            });
        }
    }
}
