package com.yudha.testandroid.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yudha.testandroid.R;
import com.yudha.testandroid.base.BaseViewAdapter;
import com.yudha.testandroid.base.BaseViewHolder;
import com.yudha.testandroid.model.ItemModel;

import java.util.List;

/**
 * Created by yudha on 2019-07-07.
 */
public class ProductAdapter extends BaseViewAdapter<ItemModel> {

    @Override
    public void setItems(List<ItemModel> items) {
        super.setItems(items);
    }

    @Override
    public BaseViewHolder<ItemModel> createHolder(ViewGroup parent, int viewType) {
        return new ProductViewHolder(R.layout.layout_home_product, parent);
    }

    @Override
    public void bindHolder(BaseViewHolder holder, int position) {
        holder.bindView(getItem(position));
    }

    public static class ProductViewHolder extends BaseViewHolder<ItemModel>{

        private ImageView ivProduct;
        private TextView tvProduct;

        public ProductViewHolder(int resId, @NonNull ViewGroup parent) {
            super(resId, parent);

            ivProduct = itemView.findViewById(R.id.home_product_image);
            tvProduct = itemView.findViewById(R.id.home_product_title);
        }

        @Override
        public void bindView(final ItemModel itemModel) {
            String[] realImageURI = itemModel.getImage().split("\\?");
            Picasso.with(itemView.getContext())
                    .load(realImageURI[0])
                    .fit()
                    .centerInside()
                    .into(ivProduct);

            tvProduct.setText(itemModel.getName());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemModel.getLink()));
                    itemView.getContext().startActivity(browserIntent);
                }
            });
        }
    }
}
