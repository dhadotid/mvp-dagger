package com.yudha.testandroid.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;


/**
 * Created by yudha on 2019-07-07.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutID());
        initItem();
    }

    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void hideActionBar(){
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null)
            actionBar.hide();
    }

    public abstract int getLayoutID();
    public abstract void initItem();
}
