package com.yudha.testandroid.base;

/**
 * Created by yudha on 2019-07-05.
 */
public interface BasePresenter {

    void start();
}
