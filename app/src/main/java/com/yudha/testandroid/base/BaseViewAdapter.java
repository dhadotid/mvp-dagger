package com.yudha.testandroid.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yudha on 2019-07-05.
 */
public abstract class BaseViewAdapter<T> extends RecyclerView.Adapter<BaseViewHolder> {

    private List<Object> items = new ArrayList<>();

    private ItemClickListener itemClickListener;

    public ItemClickListener getItemClickListener(){
        return itemClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public List<Object> getItems() {
        return items;
    }

    public Object getItem(int position){
        return items.get(position);
    }

    public void clearData(){
        int count = getItemCount();
        this.items.clear();
        notifyItemRangeRemoved(0, count);
    }

    public void setItems(List<T> items){
        this.items.addAll(items);
        notifyItemRangeInserted(0, items.size());
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return createHolder(viewGroup, i);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        bindHolder(baseViewHolder, i);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public abstract BaseViewHolder<T> createHolder(ViewGroup parent, int viewType);

    public abstract void bindHolder(BaseViewHolder holder, int position);

    public interface ItemClickListener<T>{
        void onItemClickListener(T item);
    }
}
