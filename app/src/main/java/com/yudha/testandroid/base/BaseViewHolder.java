package com.yudha.testandroid.base;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * Created by yudha on 2019-07-05.
 */
public class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    public BaseViewHolder(@LayoutRes int resId, @NonNull ViewGroup parent){
        super(LayoutInflater.from(parent.getContext()).inflate(resId, parent, false));
    }

    public void bindView(T object){}
}
