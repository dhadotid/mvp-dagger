package com.yudha.testandroid.connection.api;

import com.yudha.testandroid.connection.service.HomeService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    private String BASE_URL = "https://private-a8e48-hcidtest.apiary-mock.com/";

    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor){
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.networkInterceptors().add(loggingInterceptor);
        return httpClientBuilder.build();
    }

    @Provides
    GsonConverterFactory provideGsonConverterFactory(){
        return GsonConverterFactory.create(ApiConfig.GSON);
    }

    @Provides
    Retrofit provideRetrofit(OkHttpClient okHttpClient,
                             GsonConverterFactory converterFactory){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(converterFactory)
                .build();
    }

    @Provides
    HomeService provideHomeService(Retrofit retrofit){
        return retrofit.create(HomeService.class);
    }
}
