package com.yudha.testandroid.connection.service;

import com.yudha.testandroid.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface HomeService {

    @GET("home")
    Call<ResponseModel> getData();
}
