package com.yudha.testandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class HomeModel {

    @SerializedName("section")
    private String section;
    @SerializedName("section_title")
    private String section_title;
    @SerializedName("items")
    @Expose
    private List<Map<String, String>> items;

    public String getSection() {
        if (section!= null)
            return section;
        return "";
    }

    public String getSection_title() {
        if (section_title!=null)
            return section_title;
        return "";
    }

    public List<Map<String, String>> getItems() {
        return items;
    }
}
