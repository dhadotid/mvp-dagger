package com.yudha.testandroid.model;

/**
 * Created by yudha on 2019-07-07.
 */
public class ItemModel {

    private String name;
    private String image;
    private String link;

    public ItemModel(String name, String image, String link) {
        this.name = name;
        this.image = image;
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getLink() {
        return link;
    }
}
