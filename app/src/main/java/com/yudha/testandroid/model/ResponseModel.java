package com.yudha.testandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yudha on 2019-07-07.
 */
public class ResponseModel {

    @SerializedName("data")
    @Expose
    private List<HomeModel> data;

    public List<HomeModel> getData() {
        return data;
    }


    public void setData(List<HomeModel> data) {
        this.data = data;
    }
}
