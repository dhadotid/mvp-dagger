package com.yudha.testandroid.repository;

import com.yudha.testandroid.model.HomeModel;

import java.util.List;

/**
 * Created by yudha on 2019-07-05.
 */
public interface DataSourceCallback {

    void getContent(LoadDataCallback callback);

    interface LoadDataCallback{
        void onDataLoaded(List<HomeModel> data);
        void onNoDataLoaded();
        void onError(Throwable e);
    }
}
