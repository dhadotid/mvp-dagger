package com.yudha.testandroid.repository;

import com.yudha.testandroid.connection.service.HomeService;
import com.yudha.testandroid.repository.home.HomeRemoteDataSource;
import com.yudha.testandroid.repository.home.HomeRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    HomeRemoteDataSource provideHomeDataSource(HomeService homeService){
        return new HomeRemoteDataSource(homeService);
    }

    @Provides
    @Singleton
    HomeRepository provideHomeRepository(HomeRemoteDataSource homeRemoteDataSource){
        return new HomeRepository(homeRemoteDataSource);
    }
}
