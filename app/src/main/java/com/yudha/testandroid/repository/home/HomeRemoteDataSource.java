package com.yudha.testandroid.repository.home;

import android.util.Log;

import com.yudha.testandroid.connection.service.HomeService;
import com.yudha.testandroid.model.ResponseModel;
import com.yudha.testandroid.repository.DataSourceCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yudha on 2019-07-05.
 */
public class HomeRemoteDataSource implements DataSourceCallback {

    HomeService service;

    public HomeRemoteDataSource(HomeService service){
        this.service = service;
    }

    @Override
    public void getContent(final LoadDataCallback callback) {
        service.getData().enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful() && response.code() == 200){
                    Log.e("RETROFIT2", response.body().getData().toString());
                    callback.onDataLoaded(response.body().getData());
                }else {
                    callback.onNoDataLoaded();
                    Log.e("RETROFIT2", response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                callback.onError(t);
            }
        });
    }
}
