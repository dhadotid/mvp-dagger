package com.yudha.testandroid.repository.home;

import com.yudha.testandroid.model.HomeModel;
import com.yudha.testandroid.repository.DataSourceCallback;

import java.util.List;

/**
 * Created by yudha on 2019-07-05.
 */
public class HomeRepository implements DataSourceCallback {

    private DataSourceCallback remoteDataSource;

    public HomeRepository(DataSourceCallback remoteDataSource){
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public void getContent(final LoadDataCallback callback) {
        remoteDataSource.getContent(new LoadDataCallback() {
            @Override
            public void onDataLoaded(List<HomeModel> data) {
                callback.onDataLoaded(data);
            }

            @Override
            public void onNoDataLoaded() {
                callback.onNoDataLoaded();
            }

            @Override
            public void onError(Throwable e) {
                callback.onError(e);
            }
        });
    }
}
