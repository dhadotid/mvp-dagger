package com.yudha.testandroid.ui.activity.home;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.yudha.testandroid.Injector;
import com.yudha.testandroid.R;
import com.yudha.testandroid.base.BaseActivity;
import com.yudha.testandroid.model.HomeModel;
import com.yudha.testandroid.repository.home.HomeRepository;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by yudha on 2019-07-05.
 */
public class HomeActivity extends BaseActivity implements HomeFeedContract.View{

    private RecyclerView listMenu;
    private ProgressBar progressBar;

    @Inject
    HomeRepository repository;
    private HomeFeedContract.Presenter presenter;
    private HomeAdapter adapter;

    @Override
    public int getLayoutID() {
        return R.layout.layout_recyclerview;
    }

    @Override
    public void initItem() {
        Injector.obtain(this).inject(this);

        listMenu = findViewById(R.id.layout_recyclerview_main);
        progressBar = findViewById(R.id.progressBar);

        new HomeFeedPresenter(this, repository);

        setupRecyclerView();
    }

    private void setupRecyclerView(){
        adapter = new HomeAdapter();

        listMenu.setLayoutManager(new LinearLayoutManager(this));
        listMenu.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter!=null)
            presenter.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setPresenter(HomeFeedContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setItemToView(List<HomeModel> list) {
        adapter.clearData();
        adapter.setItems(list);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setNoDataLoadToView() {
        progressBar.setVisibility(View.GONE);
        showToast("No data load to view");
    }

    @Override
    public void setErrorToView() {
        progressBar.setVisibility(View.GONE);
        showToast("Timeout. Please check your connection");
    }
}
