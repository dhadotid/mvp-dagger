package com.yudha.testandroid.ui.activity.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.yudha.testandroid.R;
import com.yudha.testandroid.adapter.ArticleAdapter;
import com.yudha.testandroid.adapter.ProductAdapter;
import com.yudha.testandroid.base.BaseViewAdapter;
import com.yudha.testandroid.base.BaseViewHolder;
import com.yudha.testandroid.model.HomeModel;
import com.yudha.testandroid.model.ItemModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yudha on 2019-07-07.
 */
public class HomeAdapter extends BaseViewAdapter<HomeModel> {

    List<HomeModel> homeModels;

    @Override
    public void setItems(List<HomeModel> items) {
        super.setItems(items);
        homeModels = items;
    }

    @Override
    public int getItemViewType(int position) {
        if (homeModels.get(position).getSection().equals("products")){
            return 1;
        }else {
            return 2;
        }
    }

    @Override
    public BaseViewHolder<HomeModel> createHolder(ViewGroup parent, int viewType) {
        if (viewType == 1){
            return new HomeProductViewHolder(R.layout.layout_recyclerview_with_cardview, parent);
        }else{
            return new HomeArticleViewHolder(R.layout.layout_recyclerview, parent);
        }
    }

    @Override
    public void bindHolder(BaseViewHolder holder, int position) {
        if (holder instanceof HomeProductViewHolder){
            holder.bindView(getItem(position));
        }else if (holder instanceof HomeArticleViewHolder){
            holder.bindView(getItem(position));
        }
    }

    public static class HomeProductViewHolder extends BaseViewHolder<HomeModel>{

        private RecyclerView listProduct;

        private ProductAdapter adapter;

        public HomeProductViewHolder(int resId, @NonNull ViewGroup parent) {
            super(resId, parent);

            listProduct = itemView.findViewById(R.id.layout_recyclerview_with_cardview_rv);
        }

        @Override
        public void bindView(HomeModel object) {
            super.bindView(object);
            adapter = new ProductAdapter();
            listProduct.setLayoutManager(new GridLayoutManager(itemView.getContext(), 3, GridLayoutManager.VERTICAL, false));
            listProduct.setAdapter(adapter);
            List<ItemModel> itemModels = new ArrayList<>();
            for (int i = 0; i < object.getItems().size(); i++){
                itemModels.add(new ItemModel(object.getItems().get(i).get("product_name"),
                        object.getItems().get(i).get("product_image"),
                        object.getItems().get(i).get("link")));
            }

            adapter.setItems(itemModels);
        }
    }

    public static class HomeArticleViewHolder extends BaseViewHolder<HomeModel>{

        private RecyclerView listArticle;
        private ProgressBar progressBar;
        private ArticleAdapter adapter;

        public HomeArticleViewHolder(int resId, @NonNull ViewGroup parent) {
            super(resId, parent);

            listArticle = itemView.findViewById(R.id.layout_recyclerview_main);
            progressBar = itemView.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void bindView(HomeModel object) {
            super.bindView(object);
            adapter = new ArticleAdapter();
            listArticle.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
            listArticle.setAdapter(adapter);

            List<ItemModel> itemModels = new ArrayList<>();
            for (int i = 0; i < object.getItems().size(); i++){
                itemModels.add(new ItemModel(object.getItems().get(i).get("article_title"),
                        object.getItems().get(i).get("article_image"),
                        object.getItems().get(i).get("link")));
            }
            adapter.setItems(itemModels);
        }
    }
}
