package com.yudha.testandroid.ui.activity.home;

import com.yudha.testandroid.base.BasePresenter;
import com.yudha.testandroid.base.BaseView;
import com.yudha.testandroid.model.HomeModel;

import java.util.List;

/**
 * Created by yudha on 2019-07-05.
 */
public class HomeFeedContract {

    interface View extends BaseView<Presenter>{
        void setItemToView(List<HomeModel> list);
        void setNoDataLoadToView();
        void setErrorToView();
    }

    interface Presenter extends BasePresenter{
        void loadData();
    }
}
