package com.yudha.testandroid.ui.activity.home;

import android.util.Log;

import com.yudha.testandroid.model.HomeModel;
import com.yudha.testandroid.repository.DataSourceCallback;
import com.yudha.testandroid.repository.home.HomeRepository;

import java.util.List;

/**
 * Created by yudha on 2019-07-05.
 */
public class HomeFeedPresenter implements HomeFeedContract.Presenter{

    private HomeFeedContract.View view;
    private HomeRepository repository;

    public HomeFeedPresenter(HomeFeedContract.View view, HomeRepository repository){
        this.view = view;
        this.repository = repository;

        this.view.setPresenter(this);
    }

    @Override
    public void start() {
        loadData();
    }

    @Override
    public void loadData() {
        repository.getContent(new DataSourceCallback.LoadDataCallback() {
            @Override
            public void onDataLoaded(List<HomeModel> data) {
                view.setItemToView(data);
            }

            @Override
            public void onNoDataLoaded() {
                view.setNoDataLoadToView();
            }

            @Override
            public void onError(Throwable e) {
                Log.e("RETROFIT", e.getMessage());
                Log.e("RETROFIT", e.getLocalizedMessage());
                view.setErrorToView();
            }
        });
    }
}
